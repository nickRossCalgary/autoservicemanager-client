import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import axios from 'axios';
import CardWrapper from './CardWrapper';

class Customers extends Component {
  
  constructor(props) {
    super(props);
    
    this.state = {
      customers: [],
      newFirstName: '',
      newLastName: ''
    }
  }
  
  componentDidMount() {
    axios.get('http://localhost:7000/customers')
      .then(res => this.setState({customers: res.data}))
      .catch(error => console.error(error));
  }
  
  updateNewFirstName = (e) => {
    this.setState({newFirstName: e.target.value})
  };
  updateNewLastName = (e) => {
    this.setState({newLastName: e.target.value})
  };
  
  addCustomerHandler = () => {
    const newUser = {
      firstName: this.state.newFirstName,
      lastName: this.state.newLastName
    };
    axios.post('http://localhost:7000/customers', newUser)
      .then(res => {
        this.setState(({customers: [...this.state.customers, res.data]}));
        this.setState({newFirstName: ''});
        this.setState({newLastName: ''});
      })
      .catch(error => console.log(error));
  };
  
  render() {
    const cardStyle = {
      height: '90px'
    };
    
    return (
      <div className="">
        <h1>Customers</h1>
        <button type="button"
                class="btn btn-success add-button-spaced"
                data-toggle="modal" data-target="#newCustomerModal"
        >Add new Customer
        </button>
        
        {this.state.customers.map(s => {
          return (
            <CardWrapper customStyle={cardStyle} clickHandler={() => {console.log(s.id);this.props.history.push(`/customers/${s.id}`)}}>
              <h3>{s.firstName} {s.lastName}</h3>
            </CardWrapper>
          );
        })}

        <div class="modal fade" id="newCustomerModal" tabindex="-1" role="dialog"
             aria-labelledby="newCustomerModalLabel"
             aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="newCustomerModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                First Name <input type="text" class="form-control" id='new-first-name'
                                  onChange={this.updateNewFirstName}/>
                Last Name <input type="text" class="form-control" id='new-last-name'
                                 onChange={this.updateNewLastName}/>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button"
                        class="btn btn-primary"
                        data-dismiss="modal"
                        onClick={() => {this.addCustomerHandler()}}>
                  Save new Customer
                </button>
              </div>
            </div>
          </div>
        </div>
      
      </div>
    );
  }
}

export default withRouter(Customers);