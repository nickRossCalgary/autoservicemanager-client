import React, {Component} from 'react';
import axios from "axios/index";
import CardWrapper from './CardWrapper';

class Vehicle extends Component {
  constructor(props){
    super(props);
    
    this.state={
      vehicle: {
        baseId: 0,
        make: '',
        model: '',
        year: 0,
        odometer: 0,
        ownerId: 0,
        id: 0,
        batteryCapacity: 0,
        oilType: '',
        dieselFilterType: ''
      },
      services: [],
      availableServices: [],
      serviceToAdd: {}
    }
  }
  
  componentDidMount() {
    //call to get customer
    this.getVehicleServices();
    axios.get(`http://localhost:7000/services`)
      .then(res => this.setState({availableServices: res.data}))
      .catch(error => console.error(error));
  
  }
  
  getVehicleServices = () => {
    const vehicleId = this.props.match.params.vehicleId;
    axios.get(`http://localhost:7000/vehicles/${vehicleId}/services`)
      .then(res => this.setState({vehicle: res.data.vehicle, services: res.data.services}))
      .catch(error => console.error(error));
  
  };
  
  addServiceHandler = () => {
    const vehicleId = this.props.match.params.vehicleId;
    axios.post(`http://localhost:7000/vehicles/${vehicleId}/services`, [this.state.serviceToAdd.id])
      .then(res => {
        this.getVehicleServices();
      })
      .catch(error => console.log(error));
    this.setState({serviceToAdd: {}});
  };
  
  removeServiceFromVehicle = (serviceId) => {
    const vehicleId = this.props.match.params.vehicleId;
    axios.delete(`http://localhost:7000/vehicles/${vehicleId}/services/${serviceId}`)
      .then(res => {this.getVehicleServices()})
      .catch(error => console.log(error));
  };
  
  render(){
    return(
      <div>
        <button type="button"
                class="btn btn-success add-button-spaced"
                data-toggle="modal" data-target="#newServiceModal"
        >Add new Service
        </button>
        
        {this.state.services.length > 0 ?
          <div>
            <h3>make: {this.state.vehicle.make}</h3>
            <h3>model: {this.state.vehicle.model}</h3>
            <h3>year: {this.state.vehicle.year}</h3>
            <h3>odometer: {this.state.vehicle.odometer}</h3>
            {this.state.vehicle.batteryCapacity  ? <h3>battery capacity: {this.state.vehicle.batteryCapacity}</h3> : ''}
            {this.state.vehicle.oilType  ? <h3>oil type: {this.state.vehicle.oilType}</h3> : ''}
            {this.state.vehicle.dieselFilterType  ? <h3>diesel filter type: {this.state.vehicle.dieselFilterType}</h3> : ''}
      
      
            {this.state.services.map(s => {
              return(
                <CardWrapper clickHandler={()=>{}}>
                  <p>{s.title}</p>
                  <p>date of service: {s.date}</p>
                  <button type="button"
                          class="btn btn-danger add-button-spaced"
                          onClick={() => this.removeServiceFromVehicle(s.id)}
                  >Remove Service
                  </button>
                </CardWrapper>
              )
            })}
          </div>
          :
          <h2>This vehicle has no services yet</h2>
        }
  
  
  
        <div className="modal fade" id="newServiceModal" tabindex="-1" role="dialog"
             aria-labelledby="newServiceModalLabel"
             aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="newServiceModalLabel">Modal title</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                Service
                <div className="dropdown mr-1">
                  <button type="button"
                          className="btn btn-secondary dropdown-toggle"
                          id="dropdownMenuOffset"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false"
                          data-offset="10,20">
                    {this.state.serviceToAdd.title  ? this.state.serviceToAdd.title : 'Choose One...'}
                  </button>
                  <div className="dropdown-menu" aria-labelledby="dropdownMenuOffset">
                    {this.state.availableServices.map(s => {
                      return(
                        <button type="button"
                                className="dropdown-item btn-secondary"
                                onClick={() => this.setState({serviceToAdd: s})}>{s.title}</button>
                      );
                    })}
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button"
                        className="btn btn-primary"
                        data-dismiss="modal"
                        onClick={() => {this.addServiceHandler()}}>
                  Save new service
                </button>
              </div>
            </div>
          </div>
        </div>
        
      </div>

    );
  }
}

export default Vehicle;