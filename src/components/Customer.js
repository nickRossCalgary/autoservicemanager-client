import React, {Component} from 'react';
import axios from "axios/index";
import CardWrapper from './CardWrapper';

class Customer extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      newMake: '',
      newModel: '',
      newYear: 0,
      newOdometer: 0,
      newOilType: '',
      newBatteryCapacity: 0,
      newDieselFilter: '',
      selectedOption: 'gas',
      id: this.props.match.params.userId,
      electricVehicles: [],
      dieselVehicles: [],
      gasVehicles: []
    }
  }
  
  
  componentDidMount() {
    //call to get customer
    axios.get(`http://localhost:7000/customers/${this.state.id}`)
      .then(res => this.setState({firstName: res.data.firstName, lastName: res.data.lastName}))
      .catch(error => console.error(error));
    
    //get customers vehicles
    this.getVehiclesForUser();
    
  }
  
  getVehiclesForUser = () => {
    axios.get(`http://localhost:7000/customers/${this.state.id}/vehicles`)
      .then(res => this.setState({
        electricVehicles: res.data.electricVehicles,
        dieselVehicles: res.data.dieselVehicles,
        gasVehicles: res.data.gasVehicles
      }))
      .catch(error => console.error(error));
  };
  
  updateNewVehicle = (type, event) => {
    if (type === 'make') {
      this.setState({newMake: event.target.value})
    }
    else if (type === 'model') {
      this.setState({newModel: event.target.value})
    }
    else if (type === 'year') {
      this.setState({newYear: event.target.value})
    }
    else if (type === 'odometer') {
      this.setState({newOdometer: event.target.value})
    }
    
    else if (type === 'oilType') {
      this.setState({newOilType: event.target.value})
    }
    else if (type === 'batteryCapacity') {
      this.setState({newBatteryCapacity: event.target.value})
    }
    else if (type === 'dieselFilterType') {
      this.setState({newDieselFilter: event.target.value})
    }
  };
  
  handleOptionChange = (event) => {
    this.setState({selectedOption: event.target.value});
  };
  
  renderSpecificTypeOptions = () => {
    if (this.state.selectedOption === 'gas') {
      return (
        <div>
          Oil type
          <input type="text" class="form-control" id='new-oilType'
                 value={this.state.newOilType}
                 onChange={(e) => this.updateNewVehicle('oilType', e)}/>
        </div>
      )
    }
    if (this.state.selectedOption === 'diesel') {
      return (
        <div>
          Diesel filter type
          <input type="text" class="form-control" id='new-dieselfilterType'
                 value={this.state.newDieselFilter}
                 onChange={(e) => this.updateNewVehicle('dieselFilterType', e)}/>
        </div>
      )
    }
    if (this.state.selectedOption === 'electric') {
      return (
        <div>
          Battery capacity in W
          <input type="text" class="form-control" id='new-batteryCapacity'
                 value={this.state.newBatteryCapacity}
                 onChange={(e) => this.updateNewVehicle('batteryCapacity', e)}/>
        </div>
      )
    }
  };
  
  addNewVehicle = () => {
    let newVehicle = {
      make: this.state.newMake,
      model: this.state.newModel,
      year: this.state.newYear,
      type: this.state.selectedOption,
      odometer: this.state.newOdometer
    };
    
    if (this.state.selectedOption === 'gas') {
      newVehicle.oilType = this.state.newOilType
    } else if (this.state.selectedOption === 'diesel') {
      newVehicle.dieselFilter = this.state.newDieselFilter
    } else if (this.state.selectedOption === 'electric') {
      newVehicle.batteryCapacity = this.state.newBatteryCapacity
    }
    this.state.newMake = '';
    this.state.newModel = '';
    this.state.newYear = '';
    this.state.newOilType = '';
    this.state.newBatteryCapacity = '';
    this.state.newDieselFilter = '';
    this.state.selectedOption = 'gas';
    
    axios.post(`http://localhost:7000/customers/${this.state.id}/vehicles`, newVehicle)
      .then(res => {
        this.getVehiclesForUser()
      })
      .catch(error => console.log(error));
  };
  
  render() {
    return <div className="">
  

      <h3>Name: {this.state.firstName} {this.state.lastName}</h3>
      <button type="button"
              class="btn btn-success add-button-spaced"
              data-toggle="modal" data-target="#newCustomerVehicleModal"
      >Add new Vehicle
      </button>
      
      {this.state.gasVehicles.length > 0 &&
          <h3>Gas Vehicles:</h3>
      }
      
      {this.state.gasVehicles.map(g => {
        return (
          <CardWrapper clickHandler={()=>this.props.history.push(`/vehicles/${g.baseId}`)}>
            <p>Make: {g.make}</p>
            <p>Model: {g.model}</p>
            <p>Year: {g.year}</p>
            <p>Odometer: {g.odometer}</p>
            <p>Oil Type: {g.oilType}</p>
          </CardWrapper>
            
            );
      })}
      
      {this.state.dieselVehicles.length > 0 &&
          <h3>Diesel Vehicles:</h3>
      }
      {this.state.dieselVehicles.map(d => {
        return (
          <CardWrapper clickHandler={()=>this.props.history.push(`/vehicles/${d.baseId}`)}>
              <p>Make: {d.make}</p>
              <p>Model: {d.model}</p>
              <p>Year: {d.year}</p>
              <p>Odometer: {d.odometer}</p>
              <p>Diesel Filter Type: {d.diselFilter}</p>
          </CardWrapper>);
      })}
      
      {this.state.electricVehicles.length > 0 &&
          <h3>Electric Vehicles:</h3>
      }
      {this.state.electricVehicles.map(e => {
        return (
          <CardWrapper clickHandler={()=>this.props.history.push(`/vehicles/${e.baseId}`)}>
              <p>Make: {e.make}</p>
              <p>Model: {e.model}</p>
              <p>Year: {e.year}</p>
              <p>Odometer: {e.odometer}</p>
              <p>Main Battery Capacity: {e.batteryCapacity}</p>
          </CardWrapper>);
      })}
      
      
      <div class="modal fade" id="newCustomerVehicleModal" tabindex="-1" role="dialog"
           aria-labelledby="newCustomerVehicleModalLabel"
           aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="newCustomerVehicleModalLabel">Modal title</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              Make <input type="text" class="form-control" id='new-make-name'
                          value={this.state.newMake}
                          onChange={(e) => this.updateNewVehicle('make', e)}/>
              Model <input type="text" class="form-control" id='new-model-name'
                           value={this.state.newModel}
                           onChange={(e) => this.updateNewVehicle('model', e)}/>
              Year <input type="number" class="form-control" id='new-year-name'
                          value={this.state.newYear}
                          onChange={(e) => this.updateNewVehicle('year', e)}/>
              Odometer <input type="number" class="form-control" id='new-odometer-name'
                              value={this.state.newOdometer}
                              onChange={(e) => this.updateNewVehicle('odometer', e)}/>
              <div className="radio">
                <label>
                  <input type="radio" value="gas"
                         checked={this.state.selectedOption === 'gas'}
                         onChange={this.handleOptionChange}/>
                  Gas vehicle
                </label>
              </div>
              <div className="radio">
                <label>
                  <input type="radio" value="diesel"
                         checked={this.state.selectedOption === 'diesel'}
                         onChange={this.handleOptionChange}/>
                  Diesel vehicle
                </label>
              </div>
              <div className="radio">
                <label>
                  <input type="radio" value="electric"
                         checked={this.state.selectedOption === 'electric'}
                         onChange={this.handleOptionChange}/>
                  Electric vehicle
                </label>
              </div>
              
              {this.renderSpecificTypeOptions()}
            
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button"
                      class="btn btn-primary"
                      data-dismiss="modal"
                      onClick={() => {
                        this.addNewVehicle()
                      }}>
                Save new vehicle
              </button>
            </div>
          </div>
        </div>
      </div>
    
    </div>;
  }
}

export default Customer;