import React, {Component} from 'react';

class CardWrapper extends Component {
  render() {
    return (
      <div class="card card-spaced card-clickable" style={this.props.customStyle}>
        <div class="card-body" onClick={() => {this.props.clickHandler()}}>
          {this.props.children}
        </div>
      </div>
    )
  }
}

export default CardWrapper;