import React, {Component} from 'react';
import axios from "axios/index";

class Services extends Component {
  constructor(props){
    super(props);
    this.state={
      services:[
      
      ]
    }
  }
  
  componentDidMount() {
    //call to get customer
    axios.get(`http://localhost:7000/services`)
      .then(res => this.setState({services: res.data}))
      .catch(error => console.error(error));
  }
  
  render(){
    const serviceCardStyle={
      height: '200px'
    };
    
    const applicabale = service => {
      let result = [];
      if(service.gasSpecific){
        result.push(<p>applies to gas vehicles</p>);
      }
      if(service.dieselSpecific){
        result.push(<p>applies to diesel vehicles</p>);
      }
      if(service.electricSpecific){
        result.push(<p>applies to electric vehicles</p>);
      }
      return result;
    };
    return(
      <div className="">
        <h1>Available services</h1>
        {this.state.services.map(s => {
          return (<div class="card card-spaced" style={serviceCardStyle}>
            <div class="card-body">
              <h2>{s.title}</h2>
              {applicabale(s)}
            </div>
          </div>);
        })}
      </div>
    );
  }
}

export default Services;