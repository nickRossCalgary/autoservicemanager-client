import React, {Component} from 'react';
import './App.css';
import {Route, Switch} from 'react-router-dom'
import Services from './components/Services';
import Customers from './components/Customers';
import Customer from './components/Customer';
import Vehicle from './components/Vehicle';

class App extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      activePage: 'customers'
    }
  }
  
  updateLink = (name, event) => {
      this.setState({activePage: name});
  };
  
  
  render() {

    const _activeLink = () => {
      return 'nav-item nav-link active';
    };
    const _inactiveLink = () => {
      return 'nav-item nav-link';
    };
    
    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <a className="navbar-brand" href="#">Auto Service Manager</a>
        
          <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div className="navbar-nav">
              <a className={this.state.activePage === 'customers' ? _activeLink() : _inactiveLink()} href="/"
              onClick={e => this.updateLink('customers', e)}>Customers</a>
              <a className={this.state.activePage === 'services'  ? _activeLink() : _inactiveLink()} href="/services"
              onClick={e => this.updateLink('services', e)}>Services</a>
            </div>
          </div>
        </nav>
      
        <div className="container container-main selector-container">
          <Switch>
            <Route exact path='/' component={Customers}/>
            <Route path='/customers/:userId' component={Customer}/>
            <Route path='/customers' component={Customers}/>
            <Route path='/services' component={Services}/>
            <Route path='/vehicles/:vehicleId' component={Vehicle}/>
          </Switch>
        </div>
    
      </div>
    );

  }
}

export default App;
